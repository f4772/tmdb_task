import 'package:get_it/get_it.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/resources/strings.dart';
import 'package:tmdb_task/support/network_utility.dart';

class MoviesService {
  final NetworkProvider _networkProvider = GetIt.instance<NetworkProvider>();

  Future<List<Movie>> getNowPlayingMoviesList(int page) async {
    Map<String, dynamic> queryParams = {"language": "en-US", "page": page};
    dynamic response = await _networkProvider.get(
        Links.baseApiLink + Links.nowPlayingMoviesLink,
        queryParams: queryParams);
    return (response['results'] as List)
        .map<Movie>((json) => Movie.fromJson(json))
        .toList();
  }

  Future<void> addMovieToWatchList(
      int movieID, int accountID, String sessionID) async {
    var data = {"media_type": "movie", "media_id": movieID, "watchlist": true};
    Map<String, dynamic> queryParams = {
      "session_id": sessionID,
    };
    await _networkProvider.post(
        Links.baseApiLink + "account/$accountID/watchlist", data,
        queryParams: queryParams);
  }

  Future<void> removeMovieFromWatchList(
      int movieID, int accountID, String sessionID) async {
    var data = {"media_type": "movie", "media_id": movieID, "watchlist": false};
    Map<String, dynamic> queryParams = {
      "session_id": sessionID,
    };
    await _networkProvider.post(
        Links.baseApiLink + "account/$accountID/watchlist", data,
        queryParams: queryParams);
  }

  Future<List<Movie>> fetchWatchListMovies(
      int accountID, String sessionID, int page) async {
    Map<String, dynamic> queryParams = {"session_id": sessionID, "page": page};
    dynamic response = await _networkProvider.get(
        Links.baseApiLink + "account/$accountID/watchlist/movies",
        queryParams: queryParams);
    return (response['results'] as List)
        .map<Movie>((json) => Movie.fromJson(json))
        .toList();
  }
}
