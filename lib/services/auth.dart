import 'package:get_it/get_it.dart';
import 'package:tmdb_task/models/user.dart';
import 'package:tmdb_task/resources/strings.dart';
import 'package:tmdb_task/support/network_utility.dart';

class AuthService {
  final NetworkProvider _networkProvider = GetIt.instance<NetworkProvider>();

  Future<String?> createRequestToken() async {
    dynamic data =
        await _networkProvider.get(Links.baseApiLink + Links.newTokenLink);
    return data["request_token"];
  }

  Future<bool> login(String username, String password, String token) async {
    var body = {
      "username": username,
      "password": password,
      "request_token": token
    };
    dynamic data =
        await _networkProvider.post(Links.baseApiLink + Links.loginLink, body);
    return data["success"];
  }

  Future<String> createSesstion(String token) async {
    var body = {"request_token": token};
    dynamic data = await _networkProvider.post(
        Links.baseApiLink + Links.creatseSessionLink, body);
    return data["session_id"];
  }

  Future<User?> getUserData(String sessionID) async {
    Map<String,dynamic> queryParams = {
      "session_id": sessionID,
    };
    dynamic data = await _networkProvider.get(
        Links.baseApiLink + Links.getAccountLink,
        queryParams: queryParams);
    User user = User.fromJson(data);
    return user;
  }
}
