import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/UI/movie_widget.dart';
import 'package:tmdb_task/bloc/movies/bloc.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/my_app.dart';
import 'package:tmdb_task/resources/strings.dart';

class NowPlayingMoviesScreen extends StatefulWidget {
  const NowPlayingMoviesScreen({Key? key}) : super(key: key);

  @override
  State<NowPlayingMoviesScreen> createState() => NowPlayingMoviesScreenState();
}

class NowPlayingMoviesScreenState extends State<NowPlayingMoviesScreen>
    with RouteAware {
  final MoviesBloc _moviesBloc = GetIt.instance<MoviesBloc>();
  late StreamSubscription _moviesSub;
  List<Movie> nowPlayingMovies = [];
  List<Movie> watchListMovies = [];
  bool loading = true;
  ScrollController scrollController = ScrollController();
  int cursor = 1;

  @override
  void initState() {
    scrollController = ScrollController()..addListener(_scrollListener);

    _moviesSub = _moviesBloc.moviesStateSubject.listen((state) {
      if (state is NowPlayingMoviesAre) {
        setState(() {
          nowPlayingMovies.addAll(state.nowPlayingMovies);
          loading = false;
        });
      }
      if (state is WatchListMoviesAre) {
        setState(() {
          watchListMovies.addAll(state.watchListMovies);
        });
      }
    });
    _moviesBloc.dispatch(MovieListRequested(cursor));
    _moviesBloc.dispatch(WatchListRequested(cursor));
    super.initState();
  }

  @override
  void dispose() {
    _moviesSub.cancel();
    MyApp.routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    MyApp.routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void didPopNext() {
    _moviesBloc.dispatch(MovieListRequested(cursor));
    _moviesBloc.dispatch(WatchListRequested(cursor));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: InkWell(
        onTap: () {
          scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        },
        child: Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
            color: Colors.blue,
            shape: BoxShape.circle,
          ),
          child: const Icon(Icons.arrow_upward, color: Colors.white),
        ),
      ),
      appBar: AppBar(
        title: const Text(AppStrings.nowPlaying),
        automaticallyImplyLeading: false,
      ),
      body: loading
          ? _loader()
          : ListView.separated(
              controller: scrollController,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
              itemBuilder: (context, index) => MovieItem(
                  movie: nowPlayingMovies[index],
                  inWatchList: isMovieInWatchList(nowPlayingMovies[index])),
              separatorBuilder: (context, index) => const Divider(),
              itemCount: nowPlayingMovies.length,
            ),
    );
  }

  Widget _loader() {
    return const Center(
      child: SpinKitThreeBounce(
        color: Colors.blue,
        size: 20,
      ),
    );
  }

  void _scrollListener() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      setState(() {
        cursor = cursor + 1;
      });
      _moviesBloc.dispatch(MovieListRequested(cursor));
    }
  }

  bool isMovieInWatchList(Movie movie) {
    for (Movie favMovie in watchListMovies) {
      if (favMovie.id == movie.id) {
        return true;
      }
    }
    return false;
  }
}
