import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/bloc/movies/movies_bloc.dart';
import 'package:tmdb_task/bloc/movies/movies_event.dart';
import 'package:tmdb_task/main_router.gr.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/resources/strings.dart';

class FavoriteMovieItem extends StatelessWidget {
  final Movie movie;
  final bool inWatchList;
  const FavoriteMovieItem(
      {Key? key, required this.movie, required this.inWatchList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MoviesBloc _moviesBloc = GetIt.instance<MoviesBloc>();
    return InkWell(
      onTap: () {
        AutoRouter.of(context)
            .push(MovieDetailsRoute(movie: movie, inWatchList: inWatchList));
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          children: [
            Container(
              margin: const EdgeInsetsDirectional.only(end: 8),
              width: 60,
              height: 90,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(
                      Links.imageBaseLink + movie.poster_path,
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        movie.title,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      InkWell(
                        child: const Icon(Icons.remove),
                        onTap: () {
                          _moviesBloc.dispatch(
                              RemoveMovieFromWatchListTapped(movie.id));
                        },
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Text(
                      movie.overview,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
