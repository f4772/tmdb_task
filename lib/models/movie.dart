// ignore_for_file: unnecessary_this, non_constant_identifier_names

class Movie {
  int id;
  String title;
  String overview;
  String poster_path;
  String release_date;
  String original_language;

  Movie(this.id, this.title, this.overview, this.poster_path, this.release_date,
      this.original_language);

  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        json["id"] as int,
        json["title"] as String,
        json["overview"] as String,
        json["poster_path"] as String,
        json["release_date"] as String,
        json["original_language"] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': this.id,
        'title': this.title,
        'overview': this.overview,
        'poster_path': this.poster_path,
        'release_date': this.release_date,
        'original_language': this.original_language
      };
}
