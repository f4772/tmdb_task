// ignore_for_file: unnecessary_this
class User {
  int id;
  String name;
  String username;

  User(this.id, this.name, this.username);

  factory User.fromJson(Map<String, dynamic> json) => User(
        json["id"] as int,
        json["name"] as String,
        json["username"] as String,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': this.id,
        'name': this.name,
        'email': this.username,
      };
}
