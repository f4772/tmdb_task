import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/bloc/auth/auth_bloc.dart';
import 'package:tmdb_task/bloc/movies/movies_bloc.dart';
import 'package:tmdb_task/main_router.gr.dart';
import 'package:tmdb_task/services/auth.dart';
import 'package:tmdb_task/services/movies.dart';
import 'package:tmdb_task/support/network_utility.dart';
import 'package:tmdb_task/support/shared_prefrenece.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  static RouteObserver<ModalRoute<void>> routeObserver =
      RouteObserver<ModalRoute<void>>();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AppRouter _appRouter = AppRouter();

  @override
  void initState() {
    _initData();
    super.initState();
  }

  void _initData() {
    GetIt.instance.registerLazySingleton<AuthBloc>(() => AuthBloc());
    GetIt.instance.registerLazySingleton<AuthService>(() => AuthService());
    GetIt.instance.registerSingleton<SharedPreferencesProvider>(
        SharedPreferencesProvider());
    GetIt.instance.registerSingleton<NetworkProvider>(
        NetworkProvider());
    GetIt.instance.registerLazySingleton<MoviesBloc>(() => MoviesBloc());
    GetIt.instance.registerLazySingleton<MoviesService>(() => MoviesService());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerDelegate: _appRouter.delegate(
        navigatorObservers: () => [MyApp.routeObserver],
      ),
      routeInformationParser: _appRouter.defaultRouteParser(),
      builder: (context, widget) => Directionality(
        textDirection: TextDirection.ltr,
        child: widget ?? Container(),
      ),
    );
  }
}
