abstract class AuthEvent{}

class NewRequestTokenGenerated extends AuthEvent{}

class CachedSessionDataRequested extends AuthEvent{}

class LoginTapped extends AuthEvent{
  String username;
  String password;
  LoginTapped(this.username,this.password);
}