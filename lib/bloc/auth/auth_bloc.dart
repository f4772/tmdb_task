import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tmdb_task/bloc/auth/bloc.dart';
import 'package:tmdb_task/bloc/bloc.dart';
import 'package:tmdb_task/models/user.dart';
import 'package:tmdb_task/resources/strings.dart';
import 'package:tmdb_task/services/auth.dart';
import 'package:tmdb_task/support/shared_prefrenece.dart';

class AuthBloc extends BLoC<AuthEvent> {
  final PublishSubject authStateSubject = PublishSubject<AuthState>();
  final SharedPreferencesProvider _sharedPreferencesProvider =
      GetIt.instance<SharedPreferencesProvider>();
  final AuthService _authService = GetIt.instance<AuthService>();

  @override
  void dispatch(AuthEvent event) async {
    if (event is CachedSessionDataRequested) {
      await _getCachedData();
    }
    if (event is NewRequestTokenGenerated) {
      await _getRequestToken();
    }
    if (event is LoginTapped) {
      await _login(event);
    }
  }

  Future<void> _getCachedData() async {
    String? cachedSessionID = await _sharedPreferencesProvider.getSessionID();
    int? cachedAccountID = await _sharedPreferencesProvider.getAccountID();
    authStateSubject.sink.add(CachedSessionDataIs(
        accountID: cachedAccountID, sessionID: cachedSessionID));
  }

  Future<void> _login(LoginTapped event) async {
    try {
      String? token = await _authService.createRequestToken();
      if (token != null) {
        await _authService.login(event.username, event.password, token);
        String sessionID = await _authService.createSesstion(token);
        _getUserData(sessionID);
      }
    } catch (e) {
      debugPrint(e.toString());
      authStateSubject.sink.add(LoginErrorIs(AppStrings.loginError));
    }
  }

  Future<void> _getUserData(String sessionID) async {
    try {
      User? user = await _authService.getUserData(sessionID);
      if (user != null) {
        authStateSubject.sink.add(LoginSuccess());
        _sharedPreferencesProvider.setAccountID(user.id);
        _sharedPreferencesProvider.setSessionID(sessionID);
      }
    } catch (e) {
      debugPrint(e.toString());
      authStateSubject.sink.add(LoginErrorIs(AppStrings.loginError));
    }
  }

  Future<String?> _getRequestToken() async {
    String? token = await _authService.createRequestToken();
    return token;
  }
}
