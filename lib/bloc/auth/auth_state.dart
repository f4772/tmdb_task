abstract class AuthState {}

class CachedSessionDataIs extends AuthState {
  String? sessionID;
  int? accountID;
  CachedSessionDataIs({this.sessionID, this.accountID});
}

class LoginSuccess extends AuthState {}

class LoginErrorIs extends AuthState {
  String error;
  LoginErrorIs(this.error);
}
