import 'package:tmdb_task/models/movie.dart';

abstract class MoviesState{}

class NowPlayingMoviesAre extends MoviesState{
  List<Movie> nowPlayingMovies;
  NowPlayingMoviesAre(this.nowPlayingMovies);
}

class MovieAddedToWatchList extends MoviesState{}

class MovieRemovedFromWatchList extends MoviesState{
  List<Movie> watchListMovies;
  MovieRemovedFromWatchList(this.watchListMovies);
}

class WatchListMoviesAre extends MoviesState{
  List<Movie> watchListMovies;
  WatchListMoviesAre(this.watchListMovies);
}

class LoggedStatusIs extends MoviesState{
  bool isUserLogged;
  LoggedStatusIs(this.isUserLogged);
}