import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tmdb_task/bloc/bloc.dart';
import 'package:tmdb_task/bloc/movies/bloc.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/services/movies.dart';
import 'package:tmdb_task/support/shared_prefrenece.dart';

class MoviesBloc extends BLoC<MoviesEvent> {
  final PublishSubject moviesStateSubject = PublishSubject<MoviesState>();
  final MoviesService _moviesService = GetIt.instance<MoviesService>();
  final SharedPreferencesProvider _sharedPreferencesProvider =
      GetIt.instance<SharedPreferencesProvider>();

  @override
  void dispatch(MoviesEvent event) async {
    if (event is MovieListRequested) {
      await _fetchNowPlayingList(event);
    }
    if (event is AddMovieToWatchListTapped) {
      await _addMovieToWatchList(event.movieID);
    }
    if (event is RemoveMovieFromWatchListTapped) {
      await _removeMovieFromWatchList(event.movieID);
    }
    if (event is WatchListRequested) {
      await _getWatchList(event.page);
    }
    if (event is LogStatusRequested) {
      await _getLoginStatus();
    }
  }

  Future<void> _fetchNowPlayingList(MovieListRequested event) async {
    List<Movie> nowPlayingMovies =
        await _moviesService.getNowPlayingMoviesList(event.page);
    moviesStateSubject.sink.add(NowPlayingMoviesAre(nowPlayingMovies));
  }

  Future<void> _addMovieToWatchList(int movieID) async {
    int? cachedAccountID = await _sharedPreferencesProvider.getAccountID();
    String? cachedSessionID = await _sharedPreferencesProvider.getSessionID();
    await _moviesService.addMovieToWatchList(
        movieID, cachedAccountID ?? 0, cachedSessionID ?? '');
    moviesStateSubject.sink.add(MovieAddedToWatchList());
  }

  Future<void> _removeMovieFromWatchList(int movieID) async {
    int? cachedAccountID = await _sharedPreferencesProvider.getAccountID();
    String? cachedSessionID = await _sharedPreferencesProvider.getSessionID();
    await _moviesService.removeMovieFromWatchList(
        movieID, cachedAccountID ?? 0, cachedSessionID ?? '');
    List<Movie> watchList = await _moviesService.fetchWatchListMovies(
        cachedAccountID ?? 0, cachedSessionID ?? '', 1);
    moviesStateSubject.sink.add(MovieRemovedFromWatchList(watchList));
  }

  Future<void> _getWatchList(int page) async {
    int? cachedAccountID = await _sharedPreferencesProvider.getAccountID();
    String? cachedSessionID = await _sharedPreferencesProvider.getSessionID();
    List<Movie> watchList = await _moviesService.fetchWatchListMovies(
        cachedAccountID ?? 0, cachedSessionID ?? '', page);
    moviesStateSubject.sink.add(WatchListMoviesAre(watchList));
  }

  Future<void> _getLoginStatus() async {
    int? cachedAccountID = await _sharedPreferencesProvider.getAccountID();
    String? cachedSessionID = await _sharedPreferencesProvider.getSessionID();
    bool isUserLogged = cachedAccountID != null && cachedSessionID != null;
    moviesStateSubject.sink.add(LoggedStatusIs(isUserLogged));
  }
}
