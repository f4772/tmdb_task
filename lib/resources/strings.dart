class AppStrings {
  static const String appTitle = "TMDB";
  static const String isRequired = "is required";
  static const String username = "Username";
  static const String password = "Password";
  static const String login = "Login";
  static const String nowPlaying = "Now Playing";
  static const String ok = "OK";
  static const String loginError =
      "Login failed , invalid username or password";
  static const String useAnonymously = "Use app anonymously";
  static const String originalLang = "Original Language";
  static const String releaseDate = "Release date";
  static const String homeScreen = "Home Screen";
  static const String watchList = "Watch List";
  static const String noWatchlistMovies = "You have no movies in the watch list";
}

class Links {
  static const String apiKey = "31521ab741626851b73c684539c33b5a";
  static const String baseApiLink = "https://api.themoviedb.org/3/";
  static const String newTokenLink = "authentication/token/new";
  static const String loginLink = "authentication/token/validate_with_login";
  static const String creatseSessionLink = "authentication/session/new";
  static const String getAccountLink = "account";
  static const String nowPlayingMoviesLink = "movie/now_playing";
  static const String imageBaseLink = "https://image.tmdb.org/t/p/original";
}
